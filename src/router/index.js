import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: ()=> import('@/components/home'),
    },
    {
      path:'/status',
      name: 'status',
      component:()=>import('@/components/status')
    },
    {
      path:'/userdata',
      name: 'userdata',
      component:()=>import('@/components/userdata')
    },
    {
      path:'/useredit',
      name: 'useredit',
      component:()=>import('@/components/editUserInfo')
    },
  ],
  // linkActiveClass: 'active'
})
