export const mix = {
    data() {
        return {
            ws: null
        }
    },
    methods: {
        getWsUrl() {
            const token = sessionStorage.getItem('token') || ''
            const url = `ws://23.225.140.154:8888?token=${token}`
            return url
        },
    }
}
