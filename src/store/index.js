import Vuex from 'vuex'
import {mapState} from "vuex";
import Vue from 'vue'
Vue.use(Vuex)

const  store=new Vuex.Store({
  state:{
    userInfo: {},
    isManager: '',
    LoginFlag: sessionStorage.getItem("LoginFlag") === null ? true : false,
    showLr: false,
    account: sessionStorage.getItem("account"),
    nick: sessionStorage.getItem("nick"),
    img: sessionStorage.getItem("userImg") || require('@/assets/img/null.jpg'),
    userList: JSON.parse(sessionStorage.getItem('userList')) || [],
    groupId: ''
  },
  getters:{
    userInfo: (state) => state.userInfo,
    isManager: (state) => state.isManager,
    account: (state) => state.account,
    nick: (state) => state.nick,
    userImg: (state) => state.img,
    userList: (state) => state.userList,
    groupId: (state) => state.groupId,
  },
  mutations:{
    handleUserInfo: (state, info) => {
      let data = Object.assign({}, state.userInfo, info)
      state.userInfo = data
    },
    handleIsManager: (state, isManager) => {
      state.isManager = isManager
    },
    handleAccountName: (state, account) => {
      state.account = account
      sessionStorage.setItem("account", account)
    },
    handleNickName: (state, nick) => {
      state.nick = nick
      sessionStorage.setItem("nick", nick)
    },
    handleUserImg: (state, img) => {
      state.img = img
      sessionStorage.setItem("userImg", img)
    },
    handleUserList: (state, list) => {
      state.userList = list
      sessionStorage.setItem("userList", JSON.stringify(list))
    },
    handleGroupId: (state, groupId) => {
      state.groupId = groupId
    },
  }
})
export default store
