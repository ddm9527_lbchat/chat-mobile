import axios from 'axios'

const host = 'http://23.225.140.154:3001'
axios.interceptors.request.use(function(config) {
    const token = sessionStorage.getItem('token')
        // config是请求的配置对象,里面存储的有当前的请求方法、请求路径、请求头信息
    if (token) {
        // config.baseURL= host + '/',
        config.headers.Authorization = token
        config.headers['Content-Type'] = 'application/json'
    }
    return config
})


export function fetch_login(params) {
    return axios.post(host + '/login', params)
}

export function fetch_register(params) {
    return axios.post(host + '/userregister', params)
}

export function user_data(params) {
    return axios.get(host + '/viewinformation', params)
}


export function user_setting(params) {
    return axios.post(host + '/usersettings', params)
}


export function uploadImg(params) {
    return axios.post(host + '/pic/upload', params)
}

export function changePwd(params) {
    return axios.post(`http://23.225.140.154:3001/changepassword?newpassword=${params}`)
}

export function Promotion(params) {
    return axios.post(host + '/group/updatememberidentity', params)
}

export function sendRedPacket(params) {
    return axios.post(host + '/redpacket/send', params)
}

export function grabRedPacket(params) {
    return axios.post(host + '/redpacket/openpacket', params)
}

// export function checkUserMoney(params) {
//     return axios.post(`http://23.225.140.154:3001/redpacket/usermoney/${params}`)
// }

export function showRedPacketDetail(params) {
    return axios.get(`http://23.225.140.154:3001/redpacket/selectgetuser?pId=${params}`)
}

export function getAnnounce(params) {
  return axios.get(host + `/api/announcement?noticeId=${params}`)
}
