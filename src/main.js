// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import 'amfe-flexible/index.js'
import './assets/css/base.css'
import './assets/font/iconfont.css'

import { Tab, Tabs ,Icon,Button,Field,CellGroup,NoticeBar,DropdownMenu, DropdownItem,Popup} from 'vant';
import 'vant/lib/index.css';
Vue.use(Tab).use(Tabs).use(Icon).use(Button).use(Field).use(CellGroup).use(NoticeBar).use(DropdownMenu).use(DropdownItem).use(Popup);

import store from './store/index'

import vuescroll from 'vuescroll/dist/vuescroll-native';
Vue.use(vuescroll);

import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
Vue.use(Viewer)

Vue.config.productionTip = false
Vue.prototype.globalClick = function(callback) {
  document.getElementById('app').onclick = function() {
    callback()
  };
};
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
